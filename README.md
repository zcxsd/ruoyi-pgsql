# 若依框架使用

## 一、修改对应配置文件

![image-20230426092029532](%E8%8B%A5%E4%BE%9D%E6%A1%86%E6%9E%B6%E4%BD%BF%E7%94%A8.assets/image-20230426092029532.png)

```yaml
        driverClassName: org.postgresql.Driver
        druid:
            # 主库数据源
            master:
                url:  jdbc:postgresql://192.168.0.182:5438/atlaswork
                username: postgres
                password: sBqNcl7ikw4d9xob
```

application.yml修改如下：

```yaml
# 项目相关配置
atlas:
  # 名称
  name: Atlas
  # 版本
  version: 3.8.5
  # 版权年份
  copyrightYear: 2023
  # 实例演示开关
  demoEnabled: true
  # 文件路径 示例（ Windows配置D:/ruoyi/uploadPath，Linux配置 /home/ruoyi/uploadPath）
  profile: D:/ruoyi/uploadPath
  # 获取ip地址开关
  addressEnabled: false
  # 验证码类型 math 数组计算 char 字符验证
  captchaType: math

# 开发环境配置
server:
  # 服务器的HTTP端口，默认为8080
  port: 8080
  servlet:
    # 应用的访问路径
    context-path: /
  tomcat:
    # tomcat的URI编码
    uri-encoding: UTF-8
    # 连接数满后的排队数，默认为100
    accept-count: 1000
    threads:
      # tomcat最大线程数，默认为200
      max: 800
      # Tomcat启动初始化的线程数，默认值10
      min-spare: 100

# 日志配置
logging:
  level:
    com.atlas: debug
    org.springframework: warn

# 用户配置
user:
  password:
    # 密码最大错误次数
    maxRetryCount: 5
    # 密码锁定时间（默认10分钟）
    lockTime: 10

# Spring配置
spring:
  # 资源信息
  messages:
    # 国际化资源文件路径
    basename: i18n/messages
  profiles: 
    active: druid
  # 文件上传
  servlet:
     multipart:
       # 单个文件大小
       max-file-size:  10MB
       # 设置总上传的文件大小
       max-request-size:  20MB
  # 服务模块
  devtools:
    restart:
      # 热部署开关
      enabled: true
  # redis 配置
  redis:
    # 地址
    host: localhost
    # 端口，默认为6379
    port: 6379
    # 数据库索引
    database: 0
    # 密码
    password: 
    # 连接超时时间
    timeout: 10s
    lettuce:
      pool:
        # 连接池中的最小空闲连接
        min-idle: 0
        # 连接池中的最大空闲连接
        max-idle: 8
        # 连接池的最大数据库连接数
        max-active: 8
        # #连接池最大阻塞等待时间（使用负值表示没有限制）
        max-wait: -1ms

# token配置
token:
    # 令牌自定义标识
    header: Authorization
    # 令牌密钥
    secret: abcdefghijklmnopqrstuvwxyz
    # 令牌有效期（默认30分钟）
    expireTime: 30
  
# MyBatis配置
mybatis:
    # 搜索指定包别名
    typeAliasesPackage: com.atlas.**.domain
    # 配置mapper的扫描，找到所有的mapper.xml映射文件
    mapperLocations: classpath*:mapper/**/*Mapper.xml
    # 加载全局的配置文件
    configLocation: classpath:mybatis/mybatis-config.xml

# PageHelper分页插件
pagehelper:
  helperDialect: postgresql
  supportMethodsArguments: true
  params: count=countSql
  reasonable: true

# Swagger配置
swagger:
  # 是否开启swagger
  enabled: true
  # 请求前缀
  pathMapping: /dev-api

# 防止XSS攻击
xss: 
  # 过滤开关
  enabled: true
  # 排除链接（多个用逗号分隔）
  excludes: /system/notice
  # 匹配链接
  urlPatterns: /system/*,/monitor/*,/tool/*

```



## 二、mysql转pgsql

###  1、所有系统用户，角色，部门，菜单，权限模块mapper

![image-20230426091558291](%E8%8B%A5%E4%BE%9D%E6%A1%86%E6%9E%B6%E4%BD%BF%E7%94%A8.assets/image-20230426091558291.png)

### 2、修改sql的函数

- 全局替换项目中使用的sysdate()函数为now()，因为postgresql数据库没有sysdate()函数
- 全局替换项目中使用的ifnull()函数为coalesce()，因为postgresql数据库没有ifnull()函数。*注意只替换ifnull为coalesce，不带括号。*
- 修改部门查询SQL中使用到的find_in_set函数，把 find_in_set(#{deptId}, ancestors) 替换cast(#{deptId} as varchar) = any(string_to_array(ancestors,‘,’))
- 修改Mapper中 status = 0 为 status = ‘0’
- 修改SysMenuMapper.xml中的`query`为query
- 修改数据库中的默认值

​                  如：sys_user中的user_type、sex、status、del_flag的默认值



### 3、修改分页插件为：

```yaml
# PageHelper分页插件
pagehelper:
  helperDialect: postgresql
  supportMethodsArguments: true
  params: count=countSql
  reasonable: true
```

### 4、部分表主键设置(注意起始位置)

```sql
以sys_user表为例
CREATE SEQUENCE sys_user_id_seq
START WITH 4
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;
ALTER TABLE sys_user ALTER COLUMN user_id SET DEFAULT nextval( 'sys_user_id_seq' );
```

sys_user --- 需要添加自增序列的表名
id --- sys_user表的id字段
START WITH 1 --- 从1开始
INCREMENT BY 1 --- 每次自增1
NO MINVALUE --- 无最小值 （如果需要设置最小值 举例：MINVALUE 1 ）
NO MAXVALUE --- 无最大值 （如果需要设置最大值 举例：MAXVALUE 9999 ）
CACHE 1 --- 缓存为1

ALTER TABLE 表名 ALTER COLUMN (列名)  SET DEFAULT nextval( '自增序列' );

### 5、修改代码生成模块的mapper

1、相关函数修改如上

2、导入数据库表语句修改

![image-20230426094044985](%E8%8B%A5%E4%BE%9D%E6%A1%86%E6%9E%B6%E4%BD%BF%E7%94%A8.assets/image-20230426094044985.png)

```sql
<select id="selectDbTableList" parameterType="GenTable" resultMap="GenTableResult">
		SELECT table_name, obj_description(pg_class.oid) AS table_comment, create_time, update_time
		FROM information_schema.tables
		JOIN pg_class ON relname = table_name
		LEFT JOIN (
		SELECT relname, MAX(modification) AS update_time, MIN(modification) AS create_time
		FROM (
		SELECT relname, last_vacuum, last_autovacuum, last_analyze, last_autoanalyze,
		GREATEST(last_vacuum, last_autovacuum, last_analyze, last_autoanalyze) AS modification
		FROM pg_stat_all_tables
		) AS stats
		GROUP BY relname
		) AS table_stats ON table_stats.relname = table_name
		WHERE table_schema = current_schema()
		AND obj_description(pg_class.oid)  is NOT NULL
		AND table_name NOT LIKE 'pointcloud_%'
		AND table_name NOT LIKE 'raster_%'
		AND table_name NOT LIKE 'qrtz_%'
		AND table_name NOT LIKE 'gen_%'
		AND NOT EXISTS (SELECT table_name FROM gen_table WHERE gen_table.table_name = information_schema.tables.table_name)
		<if test="tableName != null and tableName != ''">
			AND lower(table_name) like lower(concat('%', #{tableName}, '%'))
		</if>
		<if test="tableComment != null and tableComment != ''">
			AND lower(table_comment) like lower(concat('%', #{tableComment}, '%'))
		</if>
		<if test="params.beginTime != null and params.beginTime != ''"><!-- 开始时间检索 -->
			AND date_format(create_time,'%y%m%d') &gt;= date_format(#{params.beginTime},'%y%m%d')
		</if>
		<if test="params.endTime != null and params.endTime != ''"><!-- 结束时间检索 -->
			AND date_format(create_time,'%y%m%d') &lt;= date_format(#{params.endTime},'%y%m%d')
		</if>
        order by create_time desc
</select>
```

```sql
<select id="selectDbTableListByNames" resultMap="GenTableResult">
		SELECT table_name, obj_description(pg_class.oid) AS table_comment, create_time, update_time
		FROM information_schema.tables
		JOIN pg_class ON relname = table_name
		LEFT JOIN (
		SELECT relname, MAX(modification) AS update_time, MIN(modification) AS create_time
		FROM (
		SELECT relname, last_vacuum, last_autovacuum, last_analyze, last_autoanalyze,
		GREATEST(last_vacuum, last_autovacuum, last_analyze, last_autoanalyze) AS modification
		FROM pg_stat_all_tables
		) AS stats
		GROUP BY relname
		) AS table_stats ON table_stats.relname = table_name
		WHERE obj_description(pg_class.oid)  is NOT NULL
		AND table_name NOT LIKE 'pointcloud_%'
		AND table_name NOT LIKE 'raster_%'
		AND table_name NOT LIKE 'qrtz_%'
		AND table_name NOT LIKE 'gen_%'
		AND table_name NOT LIKE 'qrtz_%'
		AND table_name NOT LIKE 'gen_%'
		AND table_schema = current_schema()
		AND table_name in
	    <foreach collection="array" item="name" open="(" separator="," close=")">
 			#{name}
        </foreach> 
</select>
```

3、修改代码生成功能的sql语句

![image-20230426094424832](%E8%8B%A5%E4%BE%9D%E6%A1%86%E6%9E%B6%E4%BD%BF%E7%94%A8.assets/image-20230426094424832.png)

```sql
<select id="selectDbTableColumnsByName" parameterType="String" resultMap="GenTableColumnResult">
        SELECT column_name,
               CASE WHEN is_nullable = 'NO' AND column_name NOT IN (SELECT column_name FROM information_schema.key_column_usage WHERE table_catalog = current_database() AND table_schema = 'public' AND constraint_name LIKE '%_pkey') THEN '1' ELSE NULL END AS is_required,
               CASE WHEN column_name IN (SELECT column_name FROM information_schema.key_column_usage WHERE table_catalog = current_database() AND table_schema = 'public' AND constraint_name LIKE '%_pkey') THEN '1' ELSE '0' END AS is_pk,
               ordinal_position AS sort,
               (SELECT description FROM pg_description WHERE objoid = (SELECT oid FROM pg_class WHERE relname = #{tableName}) AND objsubid = (SELECT ordinal_position FROM information_schema.columns WHERE table_name = #{tableName} AND column_name = 'column_name')) AS column_comment,
               CASE WHEN attnum = (SELECT attnum FROM pg_attribute WHERE attrelid = (SELECT oid FROM pg_class WHERE relname = #{tableName}) AND attname = 'column_name') AND attidentity = 'a' THEN '1' ELSE '0' END AS is_increment,
               data_type AS column_type
        FROM information_schema.columns
                 JOIN pg_attribute ON (attrelid = (SELECT oid FROM pg_class WHERE relname = #{tableName}) AND attname = column_name)
        WHERE table_catalog = current_database() AND table_schema = 'public' and table_name = (#{tableName})
		order by ordinal_position
</select>
```

4、其他相关修改

有些字段在从mysql导入到pgsql中会出先空格

例如：

菜单表中menu_type字段会出现后面有空格的情况

![image-20230426094716171](%E8%8B%A5%E4%BE%9D%E6%A1%86%E6%9E%B6%E4%BD%BF%E7%94%A8.assets/image-20230426094716171.png)

## 三、代码生使用与开发自己模块

1、代码生成功能



2、快速后端



3、快速前端



## 四、SpringSecurity,权限设置





## 五、定时任务模块